let nextPlayer = 'X';
let xPool = [];
let oPool = [];
const winPool = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]];
let status = 'inprogress';

// eslint-disable-next-line no-unused-vars
function blockClick (td, id) {
  if (td.innerText === '' && status === 'inprogress') {
    td.innerHTML = nextPlayer;
    putLocationInPool(parseInt(id));
    changeNextPlayer();
    checkGameStatus();
    document.getElementById('hint').innerHTML = 'Next round: ' + nextPlayer;
  }
  switch (status) {
    case 'xwin':
      document.getElementById('status').innerHTML = 'Congratulations, X wins!';
      break;
    case 'owin':
      document.getElementById('status').innerHTML = 'Congratulations, O wins!';
      break;
    case 'tie':
      document.getElementById('status').innerHTML = 'The game ends as a tie.';
  }
}

function changeNextPlayer () {
  nextPlayer === 'X' ? nextPlayer = 'O' : nextPlayer = 'X';
}

function putLocationInPool (id) {
  nextPlayer === 'X' ? xPool.push(id) : oPool.push(id);
}

function checkGameStatus () {
  winPool.forEach(element => {
    if (element.filter(v => xPool.includes(v)).length === 3) {
      status = 'xwin';
    }
  });
  winPool.forEach(element => {
    if (element.filter(v => oPool.includes(v)).length === 3) {
      status = 'owin';
    }
  });
  if (xPool.length + oPool.length === 9) {
    status = 'tie';
  }
  return status;
}

// eslint-disable-next-line no-unused-vars
function reset () {
  document.getElementById('status').innerHTML = 'Playing^_^';
  nextPlayer = 'X';
  document.getElementById('hint').innerHTML = 'Next round: ' + nextPlayer;
  document.getElementById('1').innerHTML = '';
  document.getElementById('2').innerHTML = '';
  document.getElementById('3').innerHTML = '';
  document.getElementById('4').innerHTML = '';
  document.getElementById('5').innerHTML = '';
  document.getElementById('6').innerHTML = '';
  document.getElementById('7').innerHTML = '';
  document.getElementById('8').innerHTML = '';
  document.getElementById('9').innerHTML = '';
  xPool = [];
  oPool = [];
  status = 'inprogress';
}
